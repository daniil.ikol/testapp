package com.epam.lab;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class EPAMServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(EPAMServiceApplication.class, args);
	}
}
