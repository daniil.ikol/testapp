package com.epam.lab.dto;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
@Data
@NoArgsConstructor
public class RestaurantDTO {

    private String id;
    private String name;
    private String description;
    private MenuDTO menu;


    public RestaurantDTO(String name, String description, MenuDTO menu)
    {
        this.name= name;
        this.description = description;
        this.menu = menu;
    }
    public RestaurantDTO(String id, String name, String description, MenuDTO menu)
    {
        this.id = id;
        this.name= name;
        this.description = description;
        this.menu = menu;
    }

}
