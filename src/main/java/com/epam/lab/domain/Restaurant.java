package com.epam.lab.domain;


import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@NoArgsConstructor
@Document(collection = "restaurantService")
@ToString
public class Restaurant extends Service {

    private Menu menu;

    public Restaurant(String name, String desc, Menu menu) {
        super(name, desc, ServiceType.Restaurant);
        this.menu = menu;
    }
}
