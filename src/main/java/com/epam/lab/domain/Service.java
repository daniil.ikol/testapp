package com.epam.lab.domain;


import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;

@Data
@NoArgsConstructor
public abstract class Service {

    @Id
    private String id;
    @Indexed
    private String name;
    private String description;
    private ServiceType type;

    public Service(String name, String description, ServiceType type) {
        this.name = name;
        this.description = description;
        this.type = type;
    }
}
