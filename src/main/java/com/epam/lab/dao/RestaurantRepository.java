package com.epam.lab.dao;

import com.epam.lab.domain.Restaurant;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RestaurantRepository extends MongoRepository<Restaurant, String> {
    Restaurant findByName(String restaurantName);
}
