package com.epam.lab.web.rest.controller;

import com.epam.lab.domain.Dish;
import com.epam.lab.domain.Restaurant;
import com.epam.lab.dto.DishDTO;
import com.epam.lab.dto.MenuDTO;
import com.epam.lab.dto.RestaurantDTO;
import com.epam.lab.service.RestaurantService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/restaurant")
@Slf4j
public class RestaurantController {

    @Autowired
    private RestaurantService restaurantService;

    @GetMapping
    public List<RestaurantDTO> getAll() {
        return restaurantService.getAll();
    }


    @GetMapping(value = "/{restaurantId}")
    public RestaurantDTO getById(@PathVariable final String restaurantId) {
        return restaurantService.findById(restaurantId);
    }

    @GetMapping(value = "/name/{restaurantName}")
    public RestaurantDTO getByName(@PathVariable final String restaurantName) {
        return restaurantService.findByName(restaurantName);
    }


    @GetMapping(value = "/{restaurantId}/menu")
    public MenuDTO getMenu(@PathVariable final String restaurantId) {
        return restaurantService.getMenu(restaurantId);
    }

    @GetMapping(value = "/{restaurantId}/menu/{dishName}")
    public List<DishDTO> getDishesByName(@PathVariable final String restaurantId, @PathVariable final String dishName){
        return restaurantService.findDishesByName(restaurantId, dishName);
    }

    @GetMapping(value = "/{restaurantId}/menu/price/{dishPrice}")
    public List<DishDTO> getDishesByPrice(@PathVariable final String restaurantId, @PathVariable final int dishPrice){
        return restaurantService.findDishesByPrice(restaurantId, dishPrice);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public RestaurantDTO addRestaurant(@RequestBody RestaurantDTO restaurant) {
        log.info("Restaurant :{}", restaurant);
        return restaurantService.save(restaurant);
    }

    @PostMapping(value = "/{restaurantId}/menu/dish/")
    public void addDish(@PathVariable final String restaurantId, @RequestBody Dish dish) {
        restaurantService.addDish(restaurantId, dish);
    }


    @DeleteMapping(value = "/{restaurantId}")
    public void deleteRestaurant(@PathVariable final String restaurantId) {
        restaurantService.delete(restaurantId);
    }

    @PutMapping(value = "/edit")
    public void updateRestaurant(@RequestBody Restaurant restaurant) {
        restaurantService.updateRestaurant(restaurant);
    }


//    @PostMapping("/new")
//    public void addTestRestaurant() {
//        restaurantService.save(new Restaurant("s", "s", new Menu(
//                Arrays.asList(
//                        new Dish("2", "2", 1131),
//                        new Dish("2", "2", 1131),
//                        new Dish("2", "2", 1131),
//                        new Dish("2", "2", 1131)
//                )
//        )));
//    }


}
