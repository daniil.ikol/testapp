package com.epam.lab.mapper;

import com.epam.lab.domain.Dish;
import com.epam.lab.domain.Menu;
import com.epam.lab.domain.Restaurant;
import com.epam.lab.dto.DishDTO;
import com.epam.lab.dto.MenuDTO;
import com.epam.lab.dto.RestaurantDTO;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class RestaurantMapper {

    public RestaurantDTO toDTO(Restaurant restaurant) {
        return new RestaurantDTO(
                restaurant.getId(),
                restaurant.getName(),
                restaurant.getDescription(),
                toDTO(restaurant.getMenu()));

    }

    private MenuDTO toDTO(Menu menu) {
        return MenuDTO.builder()
                .dishes(toDTO(menu.getDishes()))
                .build();
    }

    private List<DishDTO> toDTO(List<Dish> dishes) {
        return dishes.stream()
                .map(dish -> new DishDTO(dish.getName(), dish.getDescription(), dish.getPrice()))
                .collect(Collectors.toList());
    }

    public Restaurant toDomain(RestaurantDTO restaurantDTO) {
        return new Restaurant(restaurantDTO.getName(), restaurantDTO.getDescription(), toDomain(restaurantDTO.getMenu()));
    }

    private Menu toDomain(MenuDTO menu) {
        return new Menu(toDomain(menu.getDishes()));

    }

    private List<Dish> toDomain(List<DishDTO> dishes) {
        return dishes.stream()
                .map(dish -> new Dish(dish.getName(), dish.getDescription(), dish.getPrice()))
                .collect(Collectors.toList());
    }
}
