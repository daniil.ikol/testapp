package com.epam.lab.service.impl;

import com.epam.lab.dao.RestaurantRepository;
import com.epam.lab.domain.Dish;
import com.epam.lab.domain.Restaurant;
import com.epam.lab.domain.ServiceType;
import com.epam.lab.dto.DishDTO;
import com.epam.lab.dto.MenuDTO;
import com.epam.lab.dto.RestaurantDTO;
import com.epam.lab.mapper.RestaurantMapper;
import com.epam.lab.service.RestaurantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;


@Service
public class RestaurantServiceImpl implements RestaurantService {

    @Autowired
    private RestaurantRepository restaurantRepository;

    @Autowired
    private RestaurantMapper restaurantMapper;

    //GET
    @Override
    public List<RestaurantDTO> getAll() {
        return restaurantRepository.findAll().stream()
                .map(restaurant -> restaurantMapper.toDTO(restaurant))
                .collect(Collectors.toList());
    }

    @Override
    public RestaurantDTO findById(String id) {
        return restaurantMapper.toDTO(restaurantRepository.findOne(id));
    }

    @Override
    public RestaurantDTO findByName(String restaurantName) {
        return restaurantMapper.toDTO(restaurantRepository.findByName(restaurantName));
    }

    @Override
    public MenuDTO getMenu(String restaurantId) {
        return findById(restaurantId).getMenu();
    }

    @Override
    public List<DishDTO> findDishesByName(String restaurantId, String dishName) {
        return findById(restaurantId).getMenu().getDishes().stream()
                .filter(dishDTO -> dishDTO.getName().equals(dishName))
                .collect(Collectors.toList());
    }

    @Override
    public List<DishDTO> findDishesByPrice(String restaurantId, int dishPrice) {
        return findById(restaurantId).getMenu().getDishes().stream()
                .filter(dishDTO -> dishDTO.getPrice() == dishPrice)
                .collect(Collectors.toList());
    }

    //CREATE
    @Override
    public RestaurantDTO save(RestaurantDTO restaurant) {
        return restaurantMapper.toDTO(restaurantRepository.save(restaurantMapper.toDomain(restaurant)));
    }

    @Override
    public void addDish(String restaurantId, Dish dish) {

    }

    //UPDATE
    @Override
    public Restaurant updateRestaurant(Restaurant restaurantDTO) {
        Restaurant restaurant = new Restaurant();

        restaurant.setId(restaurantDTO.getId());
        restaurant.setName(restaurantDTO.getName());
        restaurant.setDescription(restaurantDTO.getDescription());
        restaurant.setMenu(restaurantDTO.getMenu());
        restaurant.setType(ServiceType.Restaurant);

        return null;
    }


    //DELETE
    @Override
    public void delete(String id) {
        restaurantRepository.delete(id);
    }


}
