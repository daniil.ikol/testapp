package com.epam.lab.service;

import com.epam.lab.domain.Dish;
import com.epam.lab.domain.Restaurant;
import com.epam.lab.dto.DishDTO;
import com.epam.lab.dto.MenuDTO;
import com.epam.lab.dto.RestaurantDTO;

import java.util.List;

public interface RestaurantService {


    //GET
    List<RestaurantDTO> getAll();

    RestaurantDTO findById(String id);

    RestaurantDTO findByName(String restaurantName);


    MenuDTO getMenu(String restaurantId);

    List<DishDTO> findDishesByName(String restaurantId, String dishName);

    List<DishDTO> findDishesByPrice(String restaurantId, int dishPrice);

    //CREATE
    RestaurantDTO save(RestaurantDTO restaurant);

    //UPDATE
    Restaurant updateRestaurant(Restaurant restaurantDTO);

    //DELETE
    void delete(String id);

    void addDish(String restaurantId, Dish dish);


}
